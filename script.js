let fb = document.querySelector(".main_form_buttons_fb"); 
let google = document.querySelector(".main_form_buttons_google"); 
let form = document.querySelector(".main_form"); 
let account = document.querySelector(".main_form_button_create");
let btn = document.querySelector(".change_colors");

let initial_color;
let initial_color2;
let initial_color3;
let initial_color4;
let color2;
let count=0;

if (localStorage.getItem("initial_color") === null) { 
    initial_color = window.getComputedStyle(fb).backgroundColor;
    localStorage.setItem("initial_color", initial_color);
    initial_color2 = window.getComputedStyle(google).backgroundColor;
    localStorage.setItem("initial_color2", initial_color2);
    initial_color3 = window.getComputedStyle(form).backgroundColor;
    localStorage.setItem("initial_color3", initial_color3);
    initial_color4 = window.getComputedStyle(account).backgroundColor;
    localStorage.setItem("initial_color4", initial_color4);
    count = 0;
}

fb.style.backgroundColor = localStorage.getItem("initial_color");
google.style.backgroundColor = localStorage.getItem("initial_color2");
form.style.backgroundColor = localStorage.getItem("initial_color3");
account.style.backgroundColor = localStorage.getItem("initial_color4");
if (localStorage.getItem("count")!==null) { 
    count = +localStorage.getItem("count");
}

btn.addEventListener("click", change_color);

function change_color() {
 
    count++;
    localStorage.setItem("count", count);
    if (count % 2 !== 0) {
        initial_color = window.getComputedStyle(fb).backgroundColor;
        fb.style.backgroundColor = "green";
        localStorage.setItem("initial_color", "green");
        localStorage.setItem("color2", initial_color);

        initial_color2 = window.getComputedStyle(google).backgroundColor;
        google.style.backgroundColor = "orange";
        localStorage.setItem("initial_color2", "orange");
        localStorage.setItem("color22", initial_color2);

        initial_color3 = window.getComputedStyle(form).backgroundColor;
        form.style.backgroundColor = "cyan";
        localStorage.setItem("initial_color3", "cyan");
        localStorage.setItem("color222", initial_color3);

        initial_color4 = window.getComputedStyle(account).backgroundColor;
        account.style.backgroundColor = "black";
        localStorage.setItem("initial_color4", "black");
        localStorage.setItem("color2222", initial_color4);

        localStorage.setItem("count", count);

    }
    else { 
        initial_color = window.getComputedStyle(fb).backgroundColor;  //green
        localStorage.setItem("initial_color", localStorage.getItem("color2")); //initial
        fb.style.backgroundColor = localStorage.getItem("initial_color");

        initial_color2 = window.getComputedStyle(google).backgroundColor;  //green
        localStorage.setItem("initial_color2", localStorage.getItem("color22")); //initial
        google.style.backgroundColor = localStorage.getItem("initial_color2");

        initial_color3 = window.getComputedStyle(form).backgroundColor;  //green
        localStorage.setItem("initial_color3", localStorage.getItem("color222")); //initial
        form.style.backgroundColor = localStorage.getItem("initial_color3");

        initial_color4 = window.getComputedStyle(account).backgroundColor;  //green
        localStorage.setItem("initial_color4", localStorage.getItem("color2222")); //initial
        account.style.backgroundColor = localStorage.getItem("initial_color4");

        localStorage.setItem("count", count);
    }
}

